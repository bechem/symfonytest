<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class NewProduct extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('productId', IntegerType::class,['attr' => ['maxlength' => 13]])
            ->add('productName', TextType::class,['attr' => ['maxlength' => 50]])
            ->add('productManager', TextType::class,[
                'attr' => ['maxlength' => 30],
                'required' => false,
            ])
            ->add('salesStartDate', DateType::class,['attr' => ['maxlength' => 10]] )
            ->add('submit', SubmitType::class)
            ->add('reset', ResetType::class)

        ;
    }
}