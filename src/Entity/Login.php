<?php

namespace App\Entity;

class Login
{
    static $userEmail = 'bechemm@gmail.com';
    static $userPassword = 'password';
    static $userName = 'Marcin Roszak';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="email")
     */
    private $email;

    /**
     * @ORM\Column(type="password", length=50)
     */
    private $password;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}