<?php

namespace App\Entity;

class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $productId;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $productName;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $productManager;

    /**
     * @ORM\Column(type="date", length=10)
     */
    private $salesStartDate;

    public function getSalesStartDate(): ?string
    {
        return $this->salesStartDate;
    }

    public function setSalesStartDate(string $salesStartDate): self
    {
        $this->salesStartDate = $salesStartDate;

        return $this;
    }

    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getProductManager(): ?string
    {
        return $this->productManager;
    }

    public function setProductManager(string $productManager): self
    {
        $this->productManager = $productManager;

        return $this;
    }


}