<?php

namespace App\Controller;

use App\Form\NewProduct;
use App\Form\LoginForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Product;
use App\Entity\Login;


class IndexController extends AbstractController
{
    protected $_JSON;
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    /** usuń jakby co
     * @Route("/welcome", name="app_test")
     */
    public function welcome(AuthenticationUtils $authenticationUtils): Response
    {
        if(!$this->isLoggedIn()){
            return $this->redirectToRoute('login_form');
        }
        return $this->render('index/welcome.html.twig');
    }

    public function save(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('index/save.html.twig');
    }

    public function new(): Response{
        if(!$this->isLoggedIn()){
            return $this->redirectToRoute('login_form');
        }
        $product = new Product();

        $form = $this->createForm(NewProduct::class, $product,[
            'action' => $this->generateUrl('save_product'),
        ]);

        return $this->render('index/new.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    public function login_form(): Response{
        $user = new Login();
        $session = $this->requestStack->getSession();

        $form = $this->createForm(LoginForm::class, $user,[
            'action' => $this->generateUrl('login'),
        ]);
        $error = null;
        if($session->get('error')){
            $error = $session->get('error');
            $session->set('error', null);
        }
        return $this->render('index/login_form.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
        ]);

    }
    public function login(Request $request){
        $loginForm = $request->request->get('login_form');
        $session = $this->requestStack->getSession();
        if($loginForm['email'] == Login::$userEmail && $loginForm['password'] == Login::$userPassword){

            $session->set('username', Login::$userName);
            $session->set('email', Login::$userEmail);
            return $this->redirectToRoute('welcome');
        }
        else{
            $session->set('error', 'Bad credentials');
            return $this->redirectToRoute('login_form');
        }
        die();
    }

    public function logout(){
        if(!$this->isLoggedIn()){
            return $this->redirectToRoute('login_form');
        }

        $session = $this->requestStack->getSession();
        $session->set('username', null);
        $session->set('email', null);
        return $this->redirectToRoute('login_form');

    }

    public function sales(){
        if(!$this->isLoggedIn()){
            return $this->redirectToRoute('login_form');
        }

        $this->getData();
        return $this->render('index/sales.html.twig');
    }

    public function data(): Response
    {
        if(!$this->isLoggedIn()){
            return $this->redirectToRoute('login_form');
        }

        // returns '{"username":"jane.doe"}' and sets the proper Content-Type header
        return $this->json($this->getData());

        // the shortcut defines three optional arguments
        // return $this->json($data, $status = 200, $headers = [], $context = []);
    }

    protected function getJSON(){
        if(!isset($this->_JSON)){
            $content = file_get_contents('../src/data/potato_sales.json'); //this file should be in better place
            $this->_JSON = json_decode($content, true);

        }
        return $this->_JSON;
    }

    protected function getHeaders(){
        $json = $this->getJSON();
        $map = [];

        foreach ($json['column'] as $column){
            if(isset($column['field'])){
                $map[$column['field']] = $column['header'];

            }
            elseif(isset($column['subHeaders'])){
                foreach ($column['subHeaders'] as $subColumn){
                    if(isset($subColumn['field'])){
                        $map[$subColumn['field']] = $subColumn['header'];
                    }
                }
            }
        }
        return $map;
    }

    protected function getData(){
        $json = $this->getJSON();
        $map = $this->getHeaders();
        $mappedData =[];
        foreach ($json['data'] as $product){
            foreach ($product as $key => $value){
                $mappedProduct[$map[$key]] = $value;
            }

            $mappedProduct['Total Sales'] = $product['salesQ1'] + $product['salesQ2'] + $product['salesQ3'] + $product['salesQ4'];
            $mappedData[] = $mappedProduct;
        }

        return $mappedData;
    }
    protected function isLoggedIn(){
        $session = $this->requestStack->getSession();
        if($session->get('username')) {
            return true;
        }
        return false;
    }
}
